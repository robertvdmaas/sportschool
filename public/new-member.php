<!DOCTYPE html>

<head>
    <title>Nieuw lid</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/member_page.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
</head>

<body>
    <div class="wrapper">
        <div class="heading">
            <header>
                <h1>Nieuw Lid</h1>
            </header>
        </div>
        <div class="sidebar">
            <div class="sidebar-members">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
        <div class="content">
            <form class="new-member-form" method="post" action="">
                <label for="name-input">Voornaam</label>
                <input id="name-input" type="text" name="first_name" placeholder="Voornaam" required/><br><br>
                <label for="name-input">Achternaam</label>
                <input id="name-input" type="text" name="last_name" placeholder="Achternaam" required/><br><br>
                <label for="gender-input">Geslacht</label>
                <input id="gender-input" type="radio" name="gender" value="m" required/>m
                <input id="gender-input" type="radio" name="gender" value="v" required/>v
                <br><br>
                <label for="postalcode-input">Postcode</label>
                <input id="postalcode-input" type="text" name="postalcode" placeholder="0000AA"
                        pattern="[0-9]{4}[A-Z]{2}" required/><br><br>
                <label for="city-input">Woonplaats</label>
                <input id="city-input" type="text" name="city" placeholder="Woonplaats" required/><br><br>
                <label for="tel-input">Telefoonnummer</label>
                <input id="tel-input" type="tel" pattern="[0-9]{10}" placeholder="0612345678" name="tel" required/><br><br>
                <label for="mail-input">Email</label>
                <input id="mail-input" type="email" name="mail" placeholder="example@example.com"
                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required/><br><br>
                 <label for="sport-input">Sport</label>
                    <select id="sport-input" name="sport">
                    <?php
                    include '../src/database/database.php';
                    include '../src/database/get.php';
                    include '../src/database/create.php';
                    include '../src/database/delete.php';

                    date_default_timezone_set("Europe/Amsterdam");

                    $db = connect();
                    $sports = getSports($db);
                    foreach ($sports as $sport) {
                        echo "<option value='$sport[sportcode]'>$sport[sportnaam] - €$sport[sportbedrag]</option>";
                    }
                    ?>
                </select><br><br>
                <label for="contribution-year">Contributiejaar</label>
                <?php
                date_default_timezone_set("Europe/Amsterdam");
                $currentYear = date("o");
                $maxYear = $currentYear + 2;
                echo "<input id='contribution-year' type='number' name='contributionYear' placeholder='$currentYear'
                    min='$currentYear' max='$maxYear' required/>";
                ?><br><br>
                <label for="payment-status-input">Betaald</label>
                <input id="payment-status-input" type="radio" name="paymentStatus" value="ja" required/>Ja
                <input id="payment-status-input" type="radio" name="paymentStatus" value="nee" required/>Nee<br><br><br>
                <input id='save-but' type="submit" value="Bevestig" name="save"/>
                <input id='cancel-but' type="button" onclick="location.href='members_page.php'" value="Terug"/>
            </form>
            <div class="resp">
                <?php

                function validateSportCode($db, $sportCode): bool {
                    $sports = getSports($db);

                    foreach ($sports as $sport) {
                        if ($sport['sportcode'] == $sportCode) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateMember($first_name, $last_name, $gender, $postalcode, $city, $tel, $mail): bool {
                    if (($first_name != "") &&
                        ($last_name != "") &&
                        ($gender != "") &&
                        ($gender == 'm' || $gender == 'v') &&
                        ($postalcode != "") &&
                        (filter_var($postalcode, FILTER_VALIDATE_REGEXP,  array("options"=>array("regexp"=>"/[0-9]{4}[A-Z]{2}/")))) &&
                        ($city != "") &&
                        ($tel != "") &&
                        (is_numeric($tel)) &&
                        (strlen($tel) == 10) &&
                        ($mail != "") &&
                        (filter_var($mail, FILTER_VALIDATE_EMAIL))) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function validatePracticedSport($db, $sportCode, $contributionYear, $paymentStatus): bool {
                    $currentYear = date("o");
                    $maxYear = $currentYear + 2;
                    if (($sportCode != "") &&
                        (validateSportCode($db, $sportCode)) &&
                        ($contributionYear != "") &&
                        ($contributionYear >= $currentYear && $contributionYear <= $maxYear) &&
                        ($paymentStatus != "") &&
                        ($paymentStatus == 'ja' || $paymentStatus == 'nee' )) {
                            return true;
                        } else {
                        return false;
                        }
                }

                if (isset($_POST["save"])) {
                    $firstName = $_POST["first_name"];
                    $lastName = $_POST["last_name"];
                    $gender = $_POST["gender"];
                    $postalCode = $_POST["postalcode"];
                    $city = $_POST["city"];
                    $tel = $_POST["tel"];
                    $mail = $_POST["mail"];
                    if (validateMember($firstName, $lastName, $gender, $postalCode, $city, $tel, $mail)) {
                        createMember($db, $firstName, $lastName, $gender, $postalCode, $city, $tel, $mail);
                        $memberID = getMemberIDByMail($db, $mail);
                        $sportCode = $_POST["sport"];
                        $sport = getSport($db, $sportCode);
                        $contribution = $sport[0]['sportbedrag'];
                        $contributionYear = $_POST["contributionYear"];
                        $paymentStatus = $_POST["paymentStatus"];
                        if (validatePracticedSport($db, $sportCode, $contributionYear, $paymentStatus)) {
                            createPractisedSport($db, $memberID, $sportCode, $contributionYear, $contribution, $paymentStatus);
                        } else {
                            deleteMember($db, $memberID);
                            echo "Invoer voor beoefende sport is niet geldig. Lid is verwijderd. Probeer opnieuw";
                        }
                    } else {
                        echo "Invoer voor nieuw lid is niet geldig. Probeer opnieuw.";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</body>