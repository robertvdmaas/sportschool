<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Sportschool</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/home.css"/>
</head>

<body>
<div class="wrapper">
    <div class="heading">
        <header>
            <h1>Sportschool</h1>
        </header>
    </div>
    <div class="sidebar">
        <div class="sidebar-home">
            <?php include 'sidebar.php'; ?>
        </div>
    </div>
    <div class="content">
        <div class="content-padding">
            <div class="content-parent">
                <div class="content-box">
                    <img id="img" alt="Afbeelding sportschool" src="https://zzpbarometer.nl/wp-content/uploads/2017/06/sportscholen-1024x573.jpg"
                    width="400"><br><br>
                    <p>Deze site is gemaakt voor een informaticaproject.<br>
                        Op deze site kan een beheerder van een sportschool administratie uitvoeren.
                        Het is wel goed om er rekening mee te houden dat de site is gemaakt aan de hand van een ERD,<br>
                        die niet volledig genoeg is voor een echte omgeving. De functionaliteiten uit de ERD zijn echter
                        zo goed mogelijk verwerkt.</p>
                    <br><br>
                    <p>De ontwikkelaars van de website en de database zijn:<br></p>
                    <ul>
                        <li>Adryan van den Berg</li>
                        <li>Jack Krijger</li>
                        <li>Robert van der Maas</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>