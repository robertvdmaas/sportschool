<!DOCTYPE html>

<head>
    <title>Nieuwe begeleider</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/trainers.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
</head>

<body>
    <div class="wrapper">
        <div class="heading">
            <header>
                <h1>Nieuwe begeleider</h1>
            </header>
        </div>
        <div class="sidebar">
            <div class="sidebar-trainers">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
        <div class="content">
            <form class="new-trainer-form" method="post" action="">
                <label for="name-input">Naam</label>
                <input id="name-input" type="text" name="name" placeholder="Volledige Naam" required/><br><br>
                <label for="gender-input">Geslacht</label>
                <input id="gender-input" type="radio" name="gender" value="m" required/>m
                <input id="gender-input" type="radio" name="gender" value="v" required/>v
                <br><br>
                <label for="tel-input">Telefoonnummer</label>
                <input id="tel-input" type="tel" pattern="[0-9]{10}" placeholder="0612345678" name="tel" required/><br><br>
                <label for="mail-input">Email</label>
                <input id="mail-input" type="email" name="mail" placeholder="example@example.com"
                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required/><br><br>
                <label for="sport-input">Sport</label>
                <select id="sport-input" name="sport">
                    <?php
                    include '../src/database/database.php';
                    include '../src/database/get.php';
                    include '../src/database/create.php';
                    include '../src/database/delete.php';

                    date_default_timezone_set("Europe/Amsterdam");

                    $db = connect();
                    $sports = getSports($db);
                    foreach ($sports as $sport) {
                        echo "<option value='$sport[sportcode]'>$sport[sportnaam]</option>";
                    }
                    ?>
                </select><br><br>
                <label for="sportyear">Sportjaar</label>
                <?php
                date_default_timezone_set("Europe/Amsterdam");
                $currentYear = date("o");
                $maxYear = $currentYear + 2;
                echo "<input id='sportyear' type='number' name='sportYear' placeholder='$currentYear'
                    min='$currentYear' max='$maxYear' required/>";
                ?><br><br>
                <label for="info">Bijzonderheden</label>
                <textarea id="info" name="info" rows="4" placeholder="Bijzonderheden hier"></textarea>
                <br><br><br>
                <input id='save-but' type="submit" value="Bevestig" name="save"/>
                <input id='cancel-but' type="button" onclick="location.href='trainers.php'" value="Terug"/>
            </form>
            <div class="resp">
                <?php

                function validateSportCode($db, $sportCode): bool {
                    $sports = getSports($db);

                    foreach ($sports as $sport) {
                        if ($sport['sportcode'] == $sportCode) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateTrainer($name, $gender, $tel, $mail): bool {
                    if (($name != "") &&
                        ($gender != "") &&
                        ($gender == 'm' || $gender == 'v') &&
                        ($tel != "") &&
                        (is_numeric($tel)) &&
                        (strlen($tel) == 10) &&
                        ($mail != "") &&
                        (filter_var($mail, FILTER_VALIDATE_EMAIL))) {
                            return true;
                    } else {
                        return false;
                    }
                }

                function validateSportTrainer($db, $sportCode, $sportYear): bool {
                    $currentYear = date("o");
                    $maxYear = $currentYear + 2;
                    if (($sportCode != "") &&
                        (validateSportCode($db, $sportCode)) &&
                        ($sportYear != "") &&
                        ($sportYear >= $currentYear && $sportYear <= $maxYear)) {
                            return true;
                    } else {
                        return false;
                    }
                }

                if (isset($_POST["save"])) {
                    $name = $_POST["name"];
                    $gender = $_POST["gender"];
                    $tel = $_POST["tel"];
                    $mail = $_POST["mail"];
                    if (validateTrainer($name, $gender, $tel, $mail)) {
                        createTrainer($db, $name, $gender, $tel, $mail);
                        $trainerID = getTrainerIDByMail($db, $mail);
                        $sportCode = $_POST["sport"];
                        $sportYear = $_POST["sportYear"];
                        $info = $_POST["info"];
                        if (validateSportTrainer($db, $sportCode, $sportYear)) {
                            createSportTrainer($db, $trainerID, $sportCode, $sportYear, $info);
                        } else {
                            deleteTrainer($db, $trainerID);
                            header("Refresh:0");
                            echo "Invoer voor sportbegeleider niet geldig. Begeleider is verwijderd. Probeer opnieuw";
                        }
                    } else {
                        header("Refresh:0");
                        echo "Invoer voor nieuwe begeleider niet geldig. Probeer opnieuw.<br>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</body>