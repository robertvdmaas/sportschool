<!DOCTYPE html>

<head>
    <title>Nieuwe sport</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/sport_page.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
</head>

<body>
    <div class="wrapper">
        <div class="heading">
            <header>
                <h1>Nieuwe sport</h1>
            </header>
        </div>
        <div class="sidebar">
            <div class="sidebar-sport">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
        <div class="content">
            <form class="new-sport-form" method="post" action="">
                <label for="name-input">Sport &nbsp</label>
                <input id="name-input" type="text" name="name" placeholder="Sportnaam" required/><br><br>
                <label for="contribution">Contributie </label>
                <input type="number" id="contribution" name="contribution" min="1" max="999.99" step="0.01" placeholder="999.99" required><br><br><br>
                <input id='save-but' type="submit" value="Bevestig" name="save"/>
                <input id='cancel-but' type="button" onclick="location.href='sport_page.php'" value="Terug"/>
            </form>
            <div class="resp">
                <?php
                include '../src/database/database.php';
                include '../src/database/create.php';
                include '../src/database/get.php';

                function validateNewSport($sport, $amount): bool {
                    $filter_opts = array('options' => array('min_range' => 1, 'max_range' => 999.99));
                    if (($sport != "") &&
                        ($amount != "") &&
                        (filter_var($amount, FILTER_VALIDATE_FLOAT, $filter_opts))) {
                        return true;
                    } else {
                        return false;
                    }
                }

                $db = connect();
                if (isset($_POST["save"])) {
                    $name = $_POST["name"];
                    $number = $_POST["contribution"];
                    if (validateNewSport($name, $number)) {
                        createSport($db, $name, $number);
                    } else {
                        echo "Invoer niet geldig. Probeer opnieuw.\n";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</body>