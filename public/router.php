<?php
    // Router.php loads the requested php/html files, like index.php.
    // This code also loads the css style correctly by changing the MIME-type to text/css.

    $path = pathinfo($_SERVER["SCRIPT_FILENAME"]);
    if ($path["extension"] == "css") {
        header("Content-Type: text/css");
        readfile($_SERVER["SCRIPT_FILENAME"]);
    } else {
        return false;
    }