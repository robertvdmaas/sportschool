<?php

// Include the file with the connection info for the database
include 'database-keys/database_connection_info.php';

// Function connect creates and returns the connection with the database.
// If the connection cannot be created, the execution is aborted with the message of the error.
function connect(): PDO {
    global $dbHost, $dbName, $dbUserName, $dbPassword;

    try {
        $connection = new PDO (
            "mysql:host=$dbHost;dbname=$dbName",
            $dbUserName, $dbPassword,
            array(
                PDO::MYSQL_ATTR_SSL_KEY => './database-keys/client-key.pem',
                PDO::MYSQL_ATTR_SSL_CERT => './database-keys/client-cert.pem',
                PDO::MYSQL_ATTR_SSL_CA => './database-keys/ca-cert.pem',
                PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false
            )
        );
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        die("Could not connect to database: " . $e->getMessage());
    }
    return $connection;
}

