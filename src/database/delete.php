<?php

// Function deleteTrainer deletes the trainer with the given ID.
function deleteTrainer($db, $id) {
    try {
        $q = $db->prepare("DELETE FROM `begeleiders` WHERE `ID_begeleider`=:id");
        $q->bindParam("id", $id);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not delete trainer: " . $e->getMessage());
    }
}

// Function deleteSportTrainer deletes the sport trainer with the given ID.
function deleteSportTrainer($db, $id) {
    try {
        $q = $db->prepare("DELETE FROM `sportbegeleider` WHERE `ID_sb`=:id");
        $q->bindParam("id", $id);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not delete sport trainer: " . $e->getMessage());
    }
}

// Function deletePractisedSport deletes the practised sport with the given unique items.
function deletePractisedSport($db, $memberID, $sportCode, $contributionYear) {
    try {
        $q = $db->prepare("DELETE FROM `beoefende_sporten` WHERE (ID_lid, sportcode, contributiejaar) = (:memberID, :sportCode, :contributionYear)");
        $q->bindParam("memberID", $memberID);
        $q->bindParam("sportCode", $sportCode);
        $q->bindParam("contributionYear", $contributionYear);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not delete practised sport: " . $e->getMessage());
    }
}

// Function deleteSport deletes a sport with the given ID.
function deleteSport($db, $id) {
    try {
        $q = $db->prepare("DELETE FROM `sporten` WHERE `sportcode`=:id");
        $q->bindParam("id", $id);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not delete sport: " . $e->getMessage());
    }
}

// Function deleteMember deletes a member with the given ID.
function deleteMember($db, $id) {
    try {
        $q = $db->prepare("DELETE FROM `leden` WHERE `ID_lid`=:id");
        $q->bindParam("id", $id);
        $q->execute();
    } catch (PDOException $e) {
        die("Could not delete member: " . $e->getMessage());
    }
}